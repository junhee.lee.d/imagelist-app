package com.example.imagelistapplication.data.repository

import android.graphics.Bitmap
import com.example.imagelistapplication.data.scrape.data.Image

interface ImageRepository {

    interface SaveImageCallback {
        fun saved()
        fun saving(percentage: Int)
        fun failed()
        fun existed()
    }

    fun get(): List<Image>
    fun save(bitmap: Bitmap, imageName: String, callback: SaveImageCallback)
}