package com.example.imagelistapplication.data.repository

import android.graphics.Bitmap
import com.example.imagelistapplication.util.isMediaMounted
import com.example.imagelistapplication.data.scrape.data.Image
import com.example.imagelistapplication.data.source.ImageDataSource

class ImageRepositorylmpl (private val imageDataSource: ImageDataSource) : ImageRepository {

    override fun get(): List<Image> = imageDataSource.get()

    override fun save(bitmap: Bitmap, imageName: String, callback: ImageRepository.SaveImageCallback) {
        if (!isMediaMounted()) return callback.failed()
        return imageDataSource.save(bitmap, imageName, callback)
    }
}