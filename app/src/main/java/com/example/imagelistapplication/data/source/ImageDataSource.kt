package com.example.imagelistapplication.data.source

import android.graphics.Bitmap
import com.example.imagelistapplication.data.repository.ImageRepository
import com.example.imagelistapplication.data.scrape.data.Image

interface ImageDataSource {
    fun get(): List<Image>
    fun save(bitmap: Bitmap, imageName: String, callback : ImageRepository.SaveImageCallback)
}