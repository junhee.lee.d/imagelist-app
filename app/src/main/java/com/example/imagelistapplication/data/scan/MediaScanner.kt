package com.example.imagelistapplication.data.scan

import android.content.Context
import android.media.MediaScannerConnection
import android.net.Uri

object MediaScanner {

    private lateinit var mediaScannerCallback: MediaScannerCallback
    private var mediaScannerConnection: MediaScannerConnection ?= null
    private lateinit var filePath: String

    interface MediaScannerCallback {
        fun onScanCompleted()
    }

    fun init(context: Context, mediaScannerCallback: MediaScannerCallback) {
        if (mediaScannerConnection == null) {
            mediaScannerConnection = MediaScannerConnection(context, mediaScannerConnectionClient)
        }
        this.mediaScannerCallback = mediaScannerCallback
    }

    fun scanFile(filePath: String) {
        this.filePath = filePath
        mediaScannerConnection!!.connect()
    }

    private val mediaScannerConnectionClient: MediaScannerConnection.MediaScannerConnectionClient
            = object : MediaScannerConnection.MediaScannerConnectionClient {

        override fun onMediaScannerConnected() {
            mediaScannerConnection!!.scanFile(filePath, null)
        }

        override fun onScanCompleted(path: String?, uri: Uri?) {
            mediaScannerConnection!!.disconnect()
            mediaScannerCallback.onScanCompleted()
        }
    }
}