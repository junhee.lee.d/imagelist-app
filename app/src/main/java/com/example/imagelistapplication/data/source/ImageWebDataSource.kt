package com.example.imagelistapplication.data.source

import android.content.Context
import android.graphics.Bitmap
import com.example.imagelistapplication.data.repository.ImageRepository
import com.example.imagelistapplication.data.save.ImageSaver
import com.example.imagelistapplication.data.scrape.WebImageScraper
import com.example.imagelistapplication.data.scrape.data.Image

class ImageWebDataSource (val context: Context,
                          private val webImageScraper: WebImageScraper = WebImageScraper(),
                          private val imageSaver: ImageSaver = ImageSaver(context)) : ImageDataSource {

    override fun get(): List<Image> {
        return webImageScraper.scrape()
    }

    override fun save(bitmap: Bitmap, imageName: String, callback: ImageRepository.SaveImageCallback) {
        imageSaver.save(bitmap, imageName, callback)
    }
}