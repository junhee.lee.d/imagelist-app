package com.example.imagelistapplication.data.scrape

import android.text.TextUtils
import com.example.imagelistapplication.data.scrape.data.Image
import org.jsoup.Jsoup
import org.jsoup.select.Elements
import java.io.IOException

class WebImageScraper {

    fun scrape(): List<Image> {
        return getImages(getElements())
    }

    private fun getElements(): Elements? {
        try {
            return Jsoup.connect(GettyImagesField.URL)
                    .userAgent("Mozilla")
                    .get()
                    .select(GettyImagesField.CLASS_NAME)
                    .select(GettyImagesField.TAG)
        } catch (e: IOException) {
            return null
        }
    }

    private fun getImages(elements: Elements?): List<Image> {
        when (elements) {
            null -> return listOf()
            else -> return getAddedImages(elements)
        }
    }

    private fun getAddedImages(elements: Elements): List<Image> {
        var images = mutableListOf<Image>()

        for (element in elements) {
            val url = element.attr(GettyImagesField.ATTR)
            if (!TextUtils.isEmpty(url)) {
                images.add(Image(url,
                        className = element.attr(GettyImagesField.ATTR1),
                        content = element.attr(GettyImagesField.ATTR2)))
            }
        }
        return images
    }
}