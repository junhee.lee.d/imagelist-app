package com.example.imagelistapplication.di

import com.example.imagelistapplication.data.repository.ImageRepository
import com.example.imagelistapplication.data.repository.ImageRepositorylmpl
import com.example.imagelistapplication.data.source.ImageDataSource
import com.example.imagelistapplication.data.source.ImageWebDataSource
import com.example.imagelistapplication.domain.usecase.GetImages
import com.example.imagelistapplication.domain.usecase.SaveImage
import com.example.imagelistapplication.presentation.detail.ImageDetailContract
import com.example.imagelistapplication.presentation.detail.ImageDetailFragment
import com.example.imagelistapplication.presentation.detail.ImageDetailPresenter
import com.example.imagelistapplication.presentation.images.ImagesContract
import com.example.imagelistapplication.presentation.images.ImagesFragment
import com.example.imagelistapplication.presentation.images.ImagesPresenter
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module


val presenterModule = module {
    factory<ImagesContract.Presenter> { (view: ImagesFragment) -> ImagesPresenter(view, get()) }
    factory<ImageDetailContract.Presenter> { (view: ImageDetailFragment) -> ImageDetailPresenter(view, get()) }
}

val useCaseModule = module {
    factory { GetImages(get()) }
    factory { SaveImage(get()) }
}

val repositoryModule = module {
    factory<ImageRepository> { ImageRepositorylmpl(get()) }
}

val dataSourceModule = module {
    factory<ImageDataSource> { ImageWebDataSource(androidContext()) }
}