package com.example.imagelistapplication.domain.usecase

import com.example.imagelistapplication.data.repository.ImageRepository
import com.example.imagelistapplication.domain.UseCase
import com.example.imagelistapplication.util.MainThreadExecutor
import com.example.imagelistapplication.util.ThreadExecutor

class GetImages (private val imageRepository: ImageRepository) : UseCase {

    fun get(callback: UseCase.GetImageCallback) {
        ThreadExecutor.executeToSingleThread(object : Runnable {
            override fun run() {
                val images = imageRepository.get()
                MainThreadExecutor.execute(object : Runnable {
                    override fun run() {
                        if (images.size > 0) {
                            callback.succcessd(images)
                        } else {
                            callback.failed()
                        }
                    }
                })
            }
        })
    }
}