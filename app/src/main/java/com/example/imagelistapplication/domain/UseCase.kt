package com.example.imagelistapplication.domain

import com.example.imagelistapplication.data.scrape.data.Image

interface UseCase {
    interface GetImageCallback {
        fun succcessd(images: List<Image>)
        fun failed()
    }

    interface SaveImageCallback {
        fun saved()
        fun saving(percentage: Int)
        fun failed()
        fun existed()
    }
}