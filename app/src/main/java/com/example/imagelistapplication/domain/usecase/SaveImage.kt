package com.example.imagelistapplication.domain.usecase

import android.graphics.Bitmap
import com.example.imagelistapplication.data.repository.ImageRepository
import com.example.imagelistapplication.domain.UseCase
import com.example.imagelistapplication.util.MainThreadExecutor
import com.example.imagelistapplication.util.ThreadExecutor

class SaveImage (private val imageRepository: ImageRepository): UseCase {

    fun save(bitmap: Bitmap, imageName: String, saveImageCallback: UseCase.SaveImageCallback) {
        ThreadExecutor.executeToSingleThread(object : Runnable {
            override fun run() {
                imageRepository.save(bitmap, imageName, object : ImageRepository.SaveImageCallback {
                    override fun saved() {
                        MainThreadExecutor.execute(object : Runnable {
                            override fun run() {
                                saveImageCallback.saved()
                            }
                        })
                    }

                    override fun saving(percentage: Int) {
                        MainThreadExecutor.execute(object : Runnable {
                            override fun run() {
                                saveImageCallback.saving(percentage)
                            }
                        })
                    }

                    override fun failed() {
                        MainThreadExecutor.execute(object : Runnable {
                            override fun run() {
                                saveImageCallback.failed()
                            }
                        })
                    }

                    override fun existed() {
                        MainThreadExecutor.execute(object : Runnable {
                            override fun run() {
                                saveImageCallback.existed()
                            }
                        })
                    }
                })
            }
        })
    }
}