package com.example.imagelistapplication.presentation.detail

import android.graphics.Bitmap

interface ImageDetailContract {

    interface View {
        fun setProgress(percentage: Int)
        fun showToast(resource: Int)
        fun showImageExistenceDialog()
    }

    interface Presenter {
        fun saveImage(bitmap: Bitmap, imageName: String)
    }
}