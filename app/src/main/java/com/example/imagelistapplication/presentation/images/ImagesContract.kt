package com.example.imagelistapplication.presentation.images

import com.example.imagelistapplication.data.scrape.data.Image

interface ImagesContract {

    interface View {
        fun setProgressBarVisibility(visibility: Int)
        fun showToast(resource: Int)
        fun setRecyclerView(images: List<Image>)
        fun closeScreen()
    }

    interface Presenter {
        fun getImages()
    }
}