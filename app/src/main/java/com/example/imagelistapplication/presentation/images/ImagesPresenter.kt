package com.example.imagelistapplication.presentation.images

import android.view.View
import com.example.imagelistapplication.R
import com.example.imagelistapplication.data.scrape.data.Image
import com.example.imagelistapplication.domain.usecase.GetImages
import com.example.imagelistapplication.domain.UseCase

class ImagesPresenter (private val view: ImagesContract.View,
                       private val getImages: GetImages)
    : ImagesContract.Presenter {

    override fun getImages() {
        view.setProgressBarVisibility(View.VISIBLE)
        getImages.get(object : UseCase.GetImageCallback {
            override fun succcessd(images: List<Image>) {
                view.setRecyclerView(images)
                view.setProgressBarVisibility(View.INVISIBLE)
                view.showToast(R.string.main_images_load_succcessd)
            }

            override fun failed() {
                view.setProgressBarVisibility(View.INVISIBLE)
                view.showToast(R.string.main_images_load_failed)
                view.closeScreen()
            }
        })
    }
}