package com.example.imagelistapplication.presentation.detail

import android.graphics.Bitmap
import com.example.imagelistapplication.R
import com.example.imagelistapplication.domain.usecase.SaveImage
import com.example.imagelistapplication.domain.UseCase

class ImageDetailPresenter (private val view: ImageDetailContract.View,
                            private val saveImage: SaveImage)
    : ImageDetailContract.Presenter {

    override fun saveImage(bitmap: Bitmap, imageName: String) {
        saveImage.save(bitmap, imageName, object : UseCase.SaveImageCallback {
            override fun saved() {
                view.showToast(R.string.image_saved_message)
            }

            override fun saving(percentage: Int) {
                view.setProgress(percentage)
            }

            override fun failed() {
                view.showToast(R.string.image_failed_message)
            }

            override fun existed() {
                view.showImageExistenceDialog()
            }
        })
    }
}