package com.example.imagelistapplication.presentation

class IntentKeys {
    companion object {
        val SELECTED_ITEM_INFO: String = "item_info"
    }
}