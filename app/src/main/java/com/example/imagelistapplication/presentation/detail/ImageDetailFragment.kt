package com.example.imagelistapplication.presentation.detail

import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.imagelistapplication.R
import com.example.imagelistapplication.databinding.FragmentImageDetailBinding
import com.example.imagelistapplication.data.scrape.data.Image
import com.example.imagelistapplication.presentation.IntentKeys
import com.example.imagelistapplication.util.ImageSetCallBack
import com.example.imagelistapplication.util.fromUrl
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class ImageDetailFragment: Fragment(), ImageDetailContract.View {

    private lateinit var dataBinding: FragmentImageDetailBinding
    private val presenter: ImageDetailContract.Presenter by inject { parametersOf(this@ImageDetailFragment) }
    lateinit var image: Image

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dataBinding = FragmentImageDetailBinding.inflate(inflater, container, false).apply {
            fragment = this@ImageDetailFragment
        }
        image = arguments!!.getSerializable(IntentKeys.SELECTED_ITEM_INFO) as Image
        setImage()
        return dataBinding.root
    }

    private fun setImage() {
        dataBinding.image.fromUrl(image.url, object : ImageSetCallBack {
            override fun callback(isSet: Boolean) {
                if (isSet) {
                    dataBinding.imageProgressbar.visibility = View.INVISIBLE
                    dataBinding.imageSaveFab.show()
                } else {
                    showToast(R.string.image_not_load_message)
                }
            }
        })
    }

    fun onImageSaveFabClick() {
        presenter.saveImage((dataBinding.image.drawable as BitmapDrawable).bitmap
                , dataBinding.imageContent.text.toString())
    }

    private fun getResourceString(resource: Int): String = resources.getString(resource)

    override fun setProgress(percentage: Int) {
        dataBinding.savingImageProgressbar.setProgress(percentage)
        dataBinding.savingImageTextview.setText("$percentage" + getResourceString(R.string.percentage_symbol))
    }

    override fun showToast(resource: Int) {
        Toast.makeText(context, getResourceString(resource), Toast.LENGTH_SHORT).show()
    }

    override fun showImageExistenceDialog() {
        AlertDialog.Builder(context!!).apply {
            setMessage(R.string.show_overwrite_image_dialog_message)
        }.create().show()
    }
}